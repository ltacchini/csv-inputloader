package inputLoaderTests;

import static org.junit.jupiter.api.Assertions.*;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.Test;

import inputLoader.CSVInputLoader;
import inputLoader.CSVInputLoaderFactory;

class CSVInputLoaderTest {
	
	@Test
	void testReadLine1() throws MalformedURLException {
		URL url1 = new URL("file:"+"aulas.txt");
		CSVInputLoader reader = new CSVInputLoader(url1);
		String columnas[] = {"id","capacidad","edíficio","NOMBRE","habilitada","tipo"};
		Object[] leidas = reader.next();
		
		assertEquals(6,leidas.length);
		for(int i=0; i<columnas.length; i++) {
			assertEquals(columnas[i],leidas[i]);
		}
	}
	
	@Test
	void testAccept() throws MalformedURLException {
//		URL url1 = new URL("file:"+"aulas.txt");
//		CSVInputLoaderFactory reader = new CSVInputLoaderFactory(url1);
//		assertTrue(reader.accept(url1));
	}

}
